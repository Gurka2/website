const color1 = "#a393bf";
const color2 = "#9882ac";
const color3 = "#73648a";
const color4 = "#453750";
const color5 = "#0c0910";
export const colors = { color1, color2, color3, color4, color5 };
