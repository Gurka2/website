export default function Home() {
  return (
    <p>
      Hello!
      <a href="../">Start page</a>
      <br></br>
      <a href="/projects/test">Test link</a>
    </p>
  );
}
