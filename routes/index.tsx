export default function Home() {
  return (
    <>
      <div
        style={{
          margin: "auto",
          marginTop: "100px",
          maxWidth: "800px",
          display: "flex",
          flexDirection: "column",
          minHeight: "90vh",
        }}
      >
        <div>
          <h1>Gurka2 / Martin</h1>
        </div>
        <div style={{ flexGrow: 1 }}>
          <p>
            Lorem ipsum is placeholder text commonly used in the graphic, print,
            and publishing industries for previewing layouts and visual mockups.
          </p>
          <a href="projects/">Projects</a>
        </div>
        <div style={{}}>
          <p>© 2024 Gurka2</p>
        </div>
      </div>
    </>
  );
}
