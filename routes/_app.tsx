import { type PageProps } from "$fresh/server.ts";
import { colors } from "../colorscheme.tsx";
export default function App({ Component }: PageProps) {
  return (
    <html>
      <head>
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link
          href="https://fonts.googleapis.com/css2?family=Roboto+Mono:ital,wght@0,100..700;1,100..700&display=swap"
          rel="stylesheet"
        />
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>website</title>
        <link rel="stylesheet" href="/styles.css" />
      </head>
      <body style={{ backgroundColor: colors.color2 }}>
        <Component />
      </body>
    </html>
  );
}
